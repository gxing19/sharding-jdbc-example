package com.gxitsky.shardingjdbc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@TableName(value = "t_order")
public class TOrder implements Serializable {
    private static final long serialVersionUID = -2757475752141112818L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 机构编码
     */
    private Integer orgCode;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 用户ID
     */
    private Long userId;

    public TOrder() {
    }

    public TOrder(String orderNo, Long userId) {
        this.orderNo = orderNo;
        this.userId = userId;
    }

    public TOrder(Integer orgCode, String orderNo, Long userId) {
        this.orgCode = orgCode;
        this.orderNo = orderNo;
        this.userId = userId;
    }
}
