package com.gxitsky.shardingjdbc.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class IdList {

    private List<Long> userIdList;
}
