package com.gxitsky.shardingjdbc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author gxing
 * @desc 订单表
 * @date 2020/9/27
 */
@Data
@Accessors(chain = true)
@TableName(value = "order_info")
public class OrderInfo implements Serializable {
    private static final long serialVersionUID = 7228334740131864851L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 机构编码
     */
    private Integer orgCode;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 用户ID
     */
    private Long userId;

    public OrderInfo() {
    }

    public OrderInfo(String orderNo, Long userId) {
        this.orderNo = orderNo;
        this.userId = userId;
    }

    public OrderInfo(Integer orgCode, String orderNo, Long userId) {
        this.orgCode = orgCode;
        this.orderNo = orderNo;
        this.userId = userId;
    }
}
