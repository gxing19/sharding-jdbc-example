package com.gxitsky.shardingjdbc.controller;

import com.gxitsky.shardingjdbc.entity.OrderInfo;
import com.gxitsky.shardingjdbc.service.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author gxing
 * @desc 订单
 * @date 2020/9/27
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    private static final Logger logger = LogManager.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    /**
     * @desc: 单个用户的订单列表(精确分表)
     * @param: []
     * @author: gxing
     * @date: 2020/9/27
     */
    @GetMapping("/userId")
    public List<OrderInfo> orderList(Long userId) {
        return orderService.orderList(userId);
    }

    @GetMapping("/list")
    public List<OrderInfo> listByOrgCodeAndUserId(Long userId, Integer orgCode) {
        return orderService.listByOrgCodeAndUserId(userId, orgCode);
    }

    /**
     * @desc: 添加订单(精确分表)
     * @param: []
     * @author: gxing
     * @date: 2020/9/27
     */
    @PostMapping("/add")
    public int addOrder(Long userId) {
        return orderService.addOrder(userId);
    }

    @PostMapping("/save")
    public int save(Long userId, Integer orgCode) {
        return orderService.save(userId, orgCode);
    }

    /**
     * @desc: 多个用户的订单列表(IN 精确分表)
     * @param: []
     * @author: gxing
     * @date: 2020/9/27
     */
    @PostMapping("/userIdList")
    public List<OrderInfo> userIdList(@RequestParam List<Long> idList) {
        return orderService.userIdList(idList);
    }

    /**
     * @desc: 大于等于(开区间全路由分表)
     * @param: [userId]
     * @author: gxing
     * @date: 2020/9/28
     */
    @PostMapping("/ge")
    public List<OrderInfo> ge(Long userId) {
        return orderService.ge(userId);
    }

    @GetMapping("/geByOrgCode")
    public List<OrderInfo> geByOrgCode(Long userId, Integer orgCode) {
        return orderService.geByOrgCode(userId, orgCode);
    }

    /**
     * @desc: 范围内(闭区间, 所有可能路由分表)
     * @param: [userId]
     * @author: gxing
     * @date: 2020/9/28
     */
    @PostMapping("/between")
    public List<OrderInfo> between(Long start, Long end) {
        return orderService.between(start, end);
    }
}
