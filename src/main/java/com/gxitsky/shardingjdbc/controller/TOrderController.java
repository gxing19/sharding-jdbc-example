package com.gxitsky.shardingjdbc.controller;

import com.gxitsky.shardingjdbc.entity.TOrder;
import com.gxitsky.shardingjdbc.service.TOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/torder")
public class TOrderController {

    @Autowired
    private TOrderService tOrderService;

    @RequestMapping("/save")
    public int save(TOrder tOrder) {
        return tOrderService.save(tOrder);
    }

}
