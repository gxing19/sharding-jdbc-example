package com.gxitsky.shardingjdbc.service;

import com.gxitsky.shardingjdbc.entity.OrderInfo;

import java.util.List;

/**
 * @author gxing
 * @desc 订单接口
 * @date 2020/9/27
 */
public interface OrderService {

    /**
     * @desc: 订单列表
     * @param: []
     * @author: gxing
     * @date: 2020/9/27
     */
    List<OrderInfo> orderList(Long userId);

    /**
     * @desc: 添加订单
     * @param: []
     * @author: gxing
     * @date: 2020/9/27
     */
    int addOrder(Long userId);

    /**
     * @desc: 多个用户订单
     * @param: []
     * @author: gxing
     * @date: 2020/9/27
     */
    List<OrderInfo> userIdList(List<Long> userIdList);

    /**
     * @desc: 大于等于
     * @param: [userId]
     * @author: gxing
     * @date: 2020/9/28
     */
    List<OrderInfo> ge(Long userId);

    /**
     * @desc: 范围查询
     * @param: [userId]
     * @author: gxing
     * @date: 2020/9/28
     */
    List<OrderInfo> between(Long start, Long end);

    /**
     * @desc: listByOrgCodeAndUserId
     * @param: [userId, orgCode]
     * @author: gxing
     * @date: 2020/9/30
     */
    List<OrderInfo> listByOrgCodeAndUserId(Long userId, Integer orgCode);

    /**
     * @desc: save
     * @param: [userId, orgCode]
     * @author: gxing
     * @date: 2020/9/30
     */
    int save(Long userId, Integer orgCode);

    List<OrderInfo> geByOrgCode(Long userId, Integer orgCode);
}
