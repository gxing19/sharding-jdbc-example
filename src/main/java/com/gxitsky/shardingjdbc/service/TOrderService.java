package com.gxitsky.shardingjdbc.service;

import com.gxitsky.shardingjdbc.entity.TOrder;

public interface TOrderService {

    /**
     * 保存
     *
     * @param tOrder
     * @return
     */
    int save(TOrder tOrder);
}
