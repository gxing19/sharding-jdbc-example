package com.gxitsky.shardingjdbc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gxitsky.shardingjdbc.entity.OrderInfo;
import com.gxitsky.shardingjdbc.mapper.OrderMapper;
import com.gxitsky.shardingjdbc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author gxing
 * @desc 订单服务
 * @date 2020/9/27
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    /**
     * @desc: 订单列表
     * @param: []
     * @author: gxing
     * @date: 2020/9/27
     */
    @Override
    public List<OrderInfo> orderList(Long userId) {
        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderInfo::getUserId, userId);
        return orderMapper.selectList(queryWrapper);
    }

    /**
     * @desc: 添加订单
     * @param: []
     * @author: gxing
     * @date: 2020/9/27
     */
    @Override
    public int addOrder(Long userId) {
        long millis = System.currentTimeMillis();
        return orderMapper.insert(new OrderInfo(String.valueOf(millis), userId));
    }


    /**
     * @desc: 多个用户的数据
     * @param: []
     * @author: gxing
     * @date: 2020/9/27
     */
    @Override
    public List<OrderInfo> userIdList(List<Long> userIdList) {
        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(OrderInfo::getUserId, userIdList);
        return orderMapper.selectList(queryWrapper);
    }

    /**
     * @desc: 大于等于
     * @param: [userId]
     * @author: gxing
     * @date: 2020/9/28
     */
    @Override
    public List<OrderInfo> ge(Long userId) {
        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.ge(OrderInfo::getUserId, userId);
        return orderMapper.selectList(queryWrapper);
    }

    /**
     * @desc: 范围查询
     * @param: [userId]
     * @author: gxing
     * @date: 2020/9/28
     */
    @Override
    public List<OrderInfo> between(Long start, Long end) {
        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.between(OrderInfo::getUserId, start, end);
        return orderMapper.selectList(queryWrapper);
    }

    @Override
    public List<OrderInfo> listByOrgCodeAndUserId(Long userId, Integer orgCode) {
        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderInfo::getUserId, userId)
                .eq(OrderInfo::getOrgCode, orgCode);
        return orderMapper.selectList(queryWrapper);
    }

    @Override
    public int save(Long userId, Integer orgCode) {
        Long millis = System.currentTimeMillis();
        return orderMapper.insert(new OrderInfo(orgCode, String.valueOf(millis), userId));
    }

    @Override
    public List<OrderInfo> geByOrgCode(Long userId, Integer orgCode) {
        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.ge(OrderInfo::getOrgCode, orgCode)
                .ge(OrderInfo::getUserId, userId);
//        queryWrapper.between(OrderInfo::getOrgCode, orgCode, orgCode + 1)
//                .between(OrderInfo::getUserId, userId, userId + 1);
        return orderMapper.selectList(queryWrapper);
    }
}
