package com.gxitsky.shardingjdbc.service.impl;

import com.alibaba.fastjson.JSON;
import com.gxitsky.shardingjdbc.entity.TOrder;
import com.gxitsky.shardingjdbc.mapper.TOrderMapper;
import com.gxitsky.shardingjdbc.service.TOrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TOrderServiceImpl implements TOrderService {
    private static final Logger logger = LogManager.getLogger(TOrderServiceImpl.class);

    @Autowired
    private TOrderMapper tOrderMapper;

    @Override
    public int save(TOrder tOrder) {
        tOrder.setOrderNo(String.valueOf(System.currentTimeMillis()));
        logger.info("----->tOrder:{}", JSON.toJSONString(tOrder));
        return tOrderMapper.insert(tOrder);
    }
}
