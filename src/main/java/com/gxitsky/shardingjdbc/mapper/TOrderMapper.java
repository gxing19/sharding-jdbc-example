package com.gxitsky.shardingjdbc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxitsky.shardingjdbc.entity.TOrder;
import org.springframework.stereotype.Repository;

@Repository
public interface TOrderMapper extends BaseMapper<TOrder> {
}
