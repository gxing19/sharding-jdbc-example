package com.gxitsky.shardingjdbc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxitsky.shardingjdbc.entity.OrderInfo;
import org.springframework.stereotype.Repository;

/**
 * @author gxing
 * @desc 订单Mapper
 * @date 2020/9/27
 */
@Repository
public interface OrderMapper extends BaseMapper<OrderInfo> {
}
